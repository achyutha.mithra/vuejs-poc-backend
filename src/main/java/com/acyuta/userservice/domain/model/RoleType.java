package com.acyuta.userservice.domain.model;

/**
 * Role Types.
 */
public enum RoleType {
    ROLE_ADMIN, ROLE_USER
}
