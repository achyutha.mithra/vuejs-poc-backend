package com.acyuta.userservice.security.service;

import com.acyuta.userservice.common.UserServiceProperties;
import com.acyuta.userservice.common.Utils;
import com.acyuta.userservice.domain.converter.RoleConverter;
import com.acyuta.userservice.domain.model.RoleType;
import com.acyuta.userservice.domain.model.User;
import com.acyuta.userservice.domain.service.UserRepository;
import com.acyuta.userservice.security.JwtUtils;
import com.acyuta.userservice.security.UserDetailsImpl;
import com.acyuta.userservice.security.dto.*;
import com.acyuta.userservice.security.dto.*;
import com.acyuta.userservice.security.enums.TokenType;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;
import java.util.UUID;

import static com.acyuta.userservice.common.Constants.*;

/**
 * AuthService Implementation.
 */
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final AuthenticationManager authenticationManager;

    private final JwtUtils jwtUtils;

    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    private final RoleConverter roleConverter;

    private final Utils utils;

    private final UserServiceProperties userServiceProperties;

    @Override
    public LoginResponseDTO login(LoginRequestDTO loginRequestDTO) {

        var authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequestDTO.getUsername(), loginRequestDTO.getPassword())
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        var userDetails = (UserDetailsImpl) authentication.getPrincipal();

        var authorityString = jwtUtils.authorityToString(userDetails.getAuthorities());

        var jwt = jwtUtils.generateJwt(userDetails.getUsername(), authorityString);

        return new LoginResponseDTO()
                .setJwt(jwt)
                .setExpiryTime(userServiceProperties.getJwtExpiryTimeInMs())
                .setRoles(authorityString)
                .setUsername(userDetails.getUsername());
    }

    @Override
    public ResponseEntity<String> signUp(SignUpRequestDTO signUpRequestDTO) {
        if (!userRepository.existsByEmailOrUsername(signUpRequestDTO.getEmail(), signUpRequestDTO.getUsername())) {
            String token = UUID.randomUUID().toString();
            var user = new User()
                    .setEmail(signUpRequestDTO.getEmail())
                    .setFirstName(signUpRequestDTO.getFirstName())
                    .setLastName(signUpRequestDTO.getLastName())
                    .setPassword(passwordEncoder.encode(signUpRequestDTO.getPassword()))
                    .setUsername(signUpRequestDTO.getUsername())
                    .setDateOfBirth(signUpRequestDTO.getDateOfBirth())
                    .setToken(token)
                    .setRoles(Set.of(roleConverter.convert(RoleType.ROLE_USER)));
            userRepository.save(user);

            utils.sendMail(
                    user.getEmail(),
                    SIGNUP_SUBJECT,
                    String.format(SIGNUP_PASSWORD_CONTENT,
                            user.getFirstName(),
                            userServiceProperties.getFrontEndUrl(),
                            token));
            return ResponseEntity.ok(String.format("New User %s created", signUpRequestDTO.getUsername()));
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, USER_ALREADY_PRESENT);
        }
    }

    @Override
    public Boolean exists(String inputValue) {
        return userRepository.existsByEmailOrUsername(inputValue, inputValue);
    }

    @Override
    public ResponseEntity<String> forgotPasswordRequest(ForgotPasswordDTO forgotPasswordDTO) {
        if (forgotPasswordDTO.getEmail() == null && forgotPasswordDTO.getUsername() == null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, USERNAME_OR_EMAIL_REQUIRED);

        var token = UUID.randomUUID().toString();
        var user = userRepository.findByUsernameOrEmail(forgotPasswordDTO.getUsername(), forgotPasswordDTO.getEmail())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, USER_NOT_FOUND));

        utils.sendMail(
                user.getEmail(),
                RESET_PASSWORD_SUBJECT,
                String.format(RESET_PASSWORD_CONTENT, userServiceProperties.getFrontEndUrl(), token));
        userRepository.save(user.setToken(token));
        return ResponseEntity.ok("password link sent.");
    }

    @Override
    public ConfirmTokenDTO confirmToken(String resetToken, TokenType tokenType) {
        var user = userRepository.findByToken(resetToken);
        if (user.isEmpty())
            return new ConfirmTokenDTO().setAuthenticated(false);
        var userObject = user.get();
        var confirmToken = new ConfirmTokenDTO()
                .setAuthenticated(true)
                .setUsername(userObject.getUsername());
        if (tokenType.equals(TokenType.signup)) {
            if (!userObject.getIsActive())
                userRepository.save(userObject.setIsActive(true).setToken(null));
            return confirmToken.setTokenType(TokenType.signup);
        }
        return confirmToken.setTokenType(TokenType.forgot_password);
    }

    @Override
    public ResponseEntity<String> resetPassword(UpdatePasswordDTO updatePasswordDTO) {
        var user = userRepository.findByUsernameAndToken(updatePasswordDTO.getUsername(), updatePasswordDTO.getToken())
                .orElseThrow(() -> new BadCredentialsException(USER_NOT_FOUND));
        userRepository.save(user.setPassword(passwordEncoder.encode(updatePasswordDTO.getNewPassword())).setToken(null));
        return ResponseEntity.ok("updated password successfully.");
    }
}
